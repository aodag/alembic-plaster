import os
import re
import sys

from alembic import config as alembic_config
from alembic import util
from alembic.util.compat import SafeConfigParser
import plaster


class PlasterConfig(alembic_config.Config):
    @util.memoized_property
    def file_config(self):
        """Return the underlying ``ConfigParser`` object.
        Direct access to the .ini file is available here,
        though the :meth:`.Config.get_section` and
        :meth:`.Config.get_main_option`
        methods provide a possibly simpler interface.
        """

        defaults = self.config_args
        file_config = SafeConfigParser(defaults)
        if self.config_file_name and os.path.exists(self.config_file_name):
            loader = plaster.get_loader(self.config_file_name)
            for section in loader.get_sections():
                if any(section.startswith(p) for p in ("logger", "handler_", "formatter_")):
                    continue
                file_config.add_section(section)
                for k, v in loader.get_settings(section=section, defaults=defaults).items():
                    file_config.set(section, k, v)
        else:
            file_config.add_section(self.config_ini_section)
        return file_config
        

class PlasterCommandLine(alembic_config.CommandLine):
    def main(self, argv=None, setup_logging=False):
        self.parser.add_argument('--paste-global', action='append',
                                 help="Define a variable passed to as "
                                 "global_conf, in the form 'var=value'")
        options = self.parser.parse_args(argv)
        if not hasattr(options, "cmd"):
            self.parser.error("too few arguments")
        else:
            if options.paste_global:
                config_args = {
                    pair[0]: pair[1] if len(pair) > 1 else ''
                    for pair in (
                        [p.strip().replace('\\=', '=') for p in re.split(r'(?<!\\)=', arg, 1)]
                        for arg in options.paste_global
                        )
                    }
            else:
                config_args = {}
            cfg = PlasterConfig(
                file_=options.config,
                ini_section=options.name,
                cmd_opts=options,
                config_args=config_args,
                )
            if setup_logging and os.path.exists(options.config):
                plaster.setup_logging(options.config)
            self.run_cmd(cfg, options)

def main(argv=None, prog=None, **kwargs):
    PlasterCommandLine(prog=prog).main(argv=argv, setup_logging=True)