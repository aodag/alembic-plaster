from setuptools import setup, find_packages

requires = [
    "alembic",
    "plaster",
    "plaster_pastedeploy",
]
setup(
    name="alembic_plaster",
    install_requires=requires,
)